import React, {Fragment} from 'react';
import './Alert.css'

 const Alert = (props) => {
    return(
         <Fragment>
             { props.show ?  <div className={`Alert ${props.type}`}>
            {props.children}
      </div> : true }
            </Fragment>
    )
 };
 export default Alert;