import React, {Fragment}  from 'react';
import './Modal.css'
const Modal = props => {
    return (
        <Fragment>
            <div
                className="Modal"
                style={{
                    transform: props.show  ? 'translateY(0)': 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <div className="modal-block-title clearfix">
                    <h2 className="title">{props.title}</h2>
                    <button className="button-close" onClick={props.closed}>X </button>
                </div>
                <div className="modal-content">
                    {props.children}
                </div>

            </div>
        </Fragment>
    );
};

export default Modal;