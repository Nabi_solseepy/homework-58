import React, { Component, Fragment } from 'react';
import './App.css';
import Modal from './component/UI/Modal/Modal'
import Alert from './component/UI/Alert/Alert'
class App extends Component {
  state = {
    modal: false,
    showAlert: true,
      showAlert2: true


  };


showModal = () => {
  this.setState({modal: true})
};
closeModal = () => {
  this.setState({modal: false})
};
showAlert = () => {
  this.setState({showAlert: true})
};

    someHandler = () =>{
      this.setState({showAlert: false})
    };
  render() {
    return (
     <Fragment>
          <button className="button" onClick={this.showModal}>show modal</button>
     <Modal show={this.state.modal}
            title="Some kinda modal title"
            closed={this.closeModal}
     >
       <p>This kinda modal title</p>
     </Modal>

         <Alert type="success" show={this.state.showAlert}>his is a success type alert <button onClick={this.someHandler } className="btn-alert">X</button></Alert>

         <Alert type="danger" show={this.state.showAlert2}> This is a success type alert</Alert>
     </Fragment>
    );
  }
}

export default App;
